<?php

namespace app\controllers;

use yii\web\Controller;

class UserController extends Controller {
    public function actionRegister() {
        return $this->render('register', [
            'message' => 'Hello from Zita',
        ]);
    }
}