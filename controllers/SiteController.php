<?php

namespace app\controllers;

use app\models\Course;
use app\models\Human;
use app\models\Teacher;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        /*
        $teacher = Teacher::find()->where(['id' => 1])->asArray()->one();
        echo "<pre>";
        var_dump($teacher);
        echo "</pre>";
        die();*/
        /*
        $t = new Teacher();
        $t->first_name = 'Vasya';
        $t->last_name = 'Petrov';
        $t->birth_date = '1990-08-01 19:15:00';
        $t->rating = 4;
        $t->save();
        var_dump(Teacher::find()->asArray()->all());
        die();
        */
        /*
        $teacher = Teacher::findOne(['id' => 1]);
        $students = $teacher->students;
        var_dump($students);
        die();
        */
        /*
        $human = new Human();
        $human->name = 'Cole';
        $human->age = 22;
        $human->description = 'Manager';
        $human->save();
        */
        /*
        $humanQuery = Human::find()->where('age = 22')->asArray()->all();
        var_dump($humanQuery);
        die();
        */
        return $this->render('index');
    }

    public function actionCreate(){
        $h = new Teacher();
        $h->setScenario('create');

        if ($h->load(Yii::$app->request->post()) && $h->validate()) {
            $h->save();
            return $this->redirect(['site/create']);
        }

        return $this->render('create', [
            'model'=>$h,
        ]);

    }

    public function actionLogin()
    {
        /*
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }


        return $this->render('login', [
            'model' => $model,
        ]);
        */

        $t = new Teacher();
        $t->setScenario('login');

        if ($t->load(Yii::$app->request->post()) && $t->validate()) {
                Yii::$app->user->login($t);
                return $this->redirect(['site/index']);
        }


        return $this->render('login', [
            'model'=>$t,
        ]);

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    public function actionFoo() {
        die('bar');    
    }
    
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionCreatecourse() {

        $c = new Course();
        $c->setScenario('createcourse');

        if ($c->load(Yii::$app->request->post()) && $c->validate()) {
            $c->save();
            return $this->redirect(['site/createcourse']);
        }

        return $this->render('createcourse', [
            'model'=>$c,
        ]);
    }
}
