<?php

use yii\widgets\ActiveForm;

$f = ActiveForm::begin();
echo $f->field($model, 'first_name');
echo $f->field($model, 'password')->passwordinput();
echo $f->field($model, 'rememberMe')->checkbox();
?>

    <input type="submit" value="Submit">

<?php
ActiveForm::end();