<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_191201_student extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('student', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'teacher_id' => Schema::TYPE_INTEGER
        ], $tableOptions);
        $this->addForeignKey('fk_teacher_student', 'student', 'teacher_id', 'teacher', 'id', 'CASCADE', 'CASCADE');

        $this->insert('student', [
            'name' => 'Vasya',
            'teacher_id' => 1
        ]);

        $this->insert('student', [
            'name' => 'Petya',
            'teacher_id' => 1
        ]);

        $this->insert('student', [
            'name' => 'Kolya',
            'teacher_id' => 1
        ]);
    }

    public function down()
    {
        //$this->dropForeignKey('fk_teacher_student', 'teacher');
        $this->dropTable('student');
    }
}
