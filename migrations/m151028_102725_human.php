<?php

use yii\db\Schema;
use yii\db\Migration;

class m151028_102725_human extends Migration
{
    public function up() {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('human', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'age' => Schema::TYPE_INTEGER,
            'description' => Schema::TYPE_TEXT
        ], $tableOptions);

        $this->insert('human', [
            'name' => 'David',
            'age' => 35,
            'description' => 'PHP Teacher'
        ]);

        $this->insert('human', [
            'name' => 'Luis',
            'age' => 20,
            'description' => 'English Teacher'
        ]);

        $this->insert('human', [
            'name' => 'Ben',
            'age' => 22,
            'description' => 'Cop'
        ]);

        $this->insert('human', [
            'name' => 'Max',
            'age' => 21,
            'description' => 'Student'
        ]);

    }

    public function down() {
        $this->dropTable('human');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
