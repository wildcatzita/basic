<?php

use yii\db\Schema;
use yii\db\Migration;

class m151102_103648_teacher_password_add extends Migration
{
    public function up()
    {

        $this->addColumn('teacher', 'password', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('teacher', 'password');
    }
}
