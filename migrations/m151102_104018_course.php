<?php

use yii\db\Schema;
use yii\db\Migration;

class m151102_104018_course extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('course', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'lessons' => Schema::TYPE_INTEGER,
            'teacher_id' => Schema::TYPE_INTEGER,
        ], $tableOptions);

        $this->insert('course', [
            'name' => 'course php',
            'lessons' => '1',
            'teacher_id' => 1,
        ]);

        $this->insert('course', [
            'name' => 'course php',
            'lessons' => '2',
            'teacher_id' => 1,
        ]);

        $this->insert('course', [
            'name' => 'course php',
            'lessons' => '3',
            'teacher_id' => 1,
        ]);

        $this->insert('course', [
            'name' => 'course php',
            'lessons' => '4',
            'teacher_id' => 1,
        ]);

        $this->addForeignKey('fk_course', 'course', 'teacher_id', 'teacher', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_course', 'course');
        $this->dropTable('course');
    }
}
