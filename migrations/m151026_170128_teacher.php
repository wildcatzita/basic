<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_170128_teacher extends Migration
{
    public function up() {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('teacher', [
            'id' => Schema::TYPE_PK,
            'first_name' => Schema::TYPE_STRING,
            'last_name' => Schema::TYPE_STRING,
            'birth_date' => Schema::TYPE_DATETIME
        ], $tableOptions);
        $this->insert('teacher', [
            'first_name' => 'Mark',
            'last_name' => 'Delay',
            'birth_date' => date('Y-m-d H:i:s')
        ]);
    }

    public function down() {
        $this->dropTable('teacher');
    }
}
