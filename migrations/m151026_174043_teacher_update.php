<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_174043_teacher_update extends Migration
{
    public function up() {
        $this->addColumn('teacher', 'rating', Schema::TYPE_FLOAT);
    }

    public function down() {
        $this->dropColumn('teacher', 'rating');
    }

}
