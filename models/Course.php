<?php

namespace app\models;

use yii\db\ActiveRecord;

class Course extends ActiveRecord{

    public $rememberMe = false;

    public static function tableName(){
        return 'course';
    }

    public function rules(){
        return [
            ['name', 'string', 'min'=>5, 'max'=>20, 'on' => 'createcourse'],
            ['lessons', 'required', 'on' => 'createcourse'],
            ['teacher_id', 'required', 'on' => 'createcourse'],
            ['teacher_id', 'exist', 'targetClass' => Teacher::className(), 'targetAttribute' => 'id', 'message' => 'teacher_id not exist'],
        ];
    }


    public function getCourse(){
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }
}