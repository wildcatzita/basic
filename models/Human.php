<?php
namespace app\models;

use yii\db\ActiveRecord;

class Human extends ActiveRecord {
    public static function tableName() {
        return 'human';
    }

    public function rules() {
        return [
            [
                'age', 'integer'
            ],
            [
                'name', 'string', 'min' => 3, 'max' => 20
            ],
            [
                'description', 'safe'
            ]
        ];
    }

    public function getStudents() {
        return $this->hasMany(Student::className(), ['teacher_id' => 'id']);
    }
}