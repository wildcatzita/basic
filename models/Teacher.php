<?php
namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class Teacher extends ActiveRecord implements IdentityInterface {

    public $rememberMe = false;

    public static function tableName() {
        return 'teacher';
    }

    public function rules(){ //http://www.yiiframework.com/doc-2.0/yii-validators-validator.html
        return [
            ['first_name', 'required', 'on'=>'create'], //��� ���� first_name � ������� teacher �������� ���������� ��� ������������ ��������
            ['last_name', 'required', 'on'=>'create'],
            ['rating', 'required', 'on'=>'create'],
            /*
            [['first_name','password'], 'required', 'on'=>'login'],
            ['last_name', 'required', ['on'=>'create', 'on'=>'login'],
            ['password', 'required', 'on'=>'login'],
            */
            ['last_name', 'string', 'min'=>5, 'max'=>10, 'on'=>'create'],
            ['birth_date', 'match', 'pattern' => '/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/'],
            //['image', 'imageSize'],
            [['first_name','password', 'rememberMe'], 'required', 'on'=>'login'],
            ['first_name', 'checkFnamePass'],
        ];
    }
    /*
    public function imageSize($attr) {
        $v = $this->$attr;
        $this->addError($attr, 'Warning Image');
    }
    */

    public function checkFnamePass () {
        $user = Teacher::findOne(['first_name' => $this->first_name, 'password' => sha1($this->password)]);
        if (is_null($user)) {
            $this->addError('first_name', 'Invalid Login or Password');
        } else {
            $this->setAttributes($user->getAttributes(), false);
        }
    }

    public function getUsername() {
        return $this->first_name;
    }

    public function getStudents() {
        return $this->hasMany(Student::className(), ['teacher_id' => 'id']);
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        //return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
}